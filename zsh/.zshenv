export LANG=en_US.UTF-8
export LC_TIME=en_DK.UTF-8
export LC_COLLATE=C
export EDITOR=vim
export SUDO_EDITOR=rvim
export CLICOLOR=true
export ZDOTDIR=$HOME/.dotfiles/zsh
alias sudo=doas