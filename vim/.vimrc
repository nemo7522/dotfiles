set backspace=2
set number
set title
set showmatch
syntax on
set tabstop=2
set shiftwidth=2
set smartindent
set autoindent
set ignorecase
set smartcase
set wrapscan

if &compatible
  set nocompatible
endif

set runtimepath+=$HOME/.vim/dein/repos/github.com/Shougo/dein.vim
call dein#begin('$HOME/.vim/dein')
call dein#add('$HOME/.vim/dein/repos/github.com/Shougo/dein.vim')
call dein#add('bronson/vim-trailing-whitespace')
call dein#end()
filetype plugin indent on
syntax enable
if dein#check_install()
	call dein#install()
endif
