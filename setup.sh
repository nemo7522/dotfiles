#!/bin/sh
ln -fn $HOME/.dotfiles/zsh/.zshenv $HOME/.zshenv
ln -fn $HOME/.dotfiles/vim/.vimrc $HOME/.vimrc
mkdir -p $HOME/.vim

curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > $HOME/installer.sh
sh $HOME/installer.sh ~/.vim/dein/
rm $HOME/installer.sh
